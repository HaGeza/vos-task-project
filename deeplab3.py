import tensorflow as tf

from tensorflow.keras.layers import \
    BatchNormalization, Conv2D, Conv2DTranspose, ReLU, \
    Concatenate, GlobalAveragePooling2D, UpSampling2D
from tensorflow.keras.applications import MobileNetV3Small, MobileNetV2
from tensorflow.keras.initializers import HeNormal

conv_size    = 3
conv_strides = 1
tr_strides   = 2
conv_filters = 256

def ConvolutionBlock(
		input,
		num_filters=conv_filters,
		kernel_size=conv_size,
        strides=conv_strides,
		dilation_rate=1,
		use_bias=False,
        use_batch_norm=True):
    
    x = Conv2D(
        num_filters, kernel_size, strides, 'same', use_bias=use_bias,
        dilation_rate=dilation_rate, kernel_initializer=HeNormal(),
        kernel_regularizer='l2'
    )(input)
    if use_batch_norm:
        x = BatchNormalization()(x)
    return ReLU()(x)
    
def TransposeConvBlock(
        input,
        num_filters=conv_filters,
        kernel_size=conv_size,
        strides=tr_strides,
        use_bias=True,
        use_batch_norm=True):
    
    x = Conv2DTranspose(
        num_filters, kernel_size, strides, 'same',
        use_bias=use_bias, kernel_initializer=HeNormal(),
        kernel_regularizer='l2'
    )(input)
    if use_batch_norm:
        x = BatchNormalization()(x)
    return ReLU()(x)
    
    
def AtrousSpatialPyramidPooling(inputs, num_filters=conv_filters, 
                                kernel_size=conv_size, strides=conv_strides,
                                dilation_rates=[6, 12, 18]):
    height, width = inputs.shape[1:3]
    
    conv_1x1 = ConvolutionBlock(inputs, num_filters, 1, strides)
    
    dilated_convs = [
        ConvolutionBlock(inputs, num_filters, kernel_size, strides, rate) 
            for rate in dilation_rates
    ]
    
    img_lvl_features = GlobalAveragePooling2D(keepdims=True)(inputs)
    img_lvl_features = ConvolutionBlock(img_lvl_features, num_filters, 1, use_bias=True)
    img_lvl_features = UpSampling2D((height, width), interpolation='bilinear')(img_lvl_features)
    
    outputs = Concatenate()([conv_1x1, *dilated_convs, img_lvl_features])
    return ConvolutionBlock(outputs, num_filters, 1)

        
aspp_layers = ['block_6_expand_relu', 'block_13_expand_relu', 'block_13_expand_relu']
low_layers  = ['block_3_expand_relu', 'block_6_expand_relu',  'block_3_expand_relu']
aspp_oss    = [8, 16, 16]
low_oss     = [4, 8,  4]
        
def DeepLabV3KA(input_shape, num_classes, num_filters=conv_filters, layer_index=0,
                freeze_mobilenet=True, activation=None, use_tr_conv=True):
    inputs = tf.keras.Input(shape=input_shape)
    mobilenet = MobileNetV2(
        input_shape=input_shape, include_top=False, weights='imagenet'
    )
    
    down_stack = tf.keras.Model(
        inputs=mobilenet.input, outputs=[
            mobilenet.get_layer(aspp_layers[layer_index]).output,
            mobilenet.get_layer(low_layers[layer_index]).output
        ]) 
    if freeze_mobilenet:
        down_stack.trainable = False
    
    x = tf.keras.applications.mobilenet_v2.preprocess_input(inputs)
    encoder_output = down_stack(x) # Batch normalization training can be tricky, may need to set training=False
    
    x = AtrousSpatialPyramidPooling(encoder_output[0], num_filters)
    input_from_aspp = UpSampling2D(
        aspp_oss[layer_index] // low_oss[layer_index],
        interpolation='bilinear'
    )(x)
    
    input_from_mobilenet = ConvolutionBlock(encoder_output[1], conv_filters // 4, 1)
    
    x = Concatenate()([input_from_aspp, input_from_mobilenet])
    
    x = ConvolutionBlock(x, num_filters)
    x = ConvolutionBlock(x, num_filters)
    
    if use_tr_conv:
        x = UpSampling2D(low_oss[layer_index] // 2, interpolation='bilinear')(x)
        x = Conv2DTranspose(num_filters, conv_size, tr_strides, 'same')(x)
    else:
        x = UpSampling2D(low_oss[layer_index], interpolation='bilinear')(x)
    
    out_filters = num_classes if num_classes > 2 else 1
    outputs = Conv2D(out_filters, conv_size, activation=activation, # could try using kernel_size=1
                    padding='same', kernel_initializer=HeNormal(),
                    kernel_regularizer='l2')(x)
    
    model = tf.keras.Model(inputs, outputs)
    return model
        