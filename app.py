import glob
import numpy as np
import tensorflow as tf
from tensorflow.keras.losses import CategoricalCrossentropy as cce, BinaryCrossentropy as bce
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input

from wrapper import *
from util.metrics import *
from util.paths import find_models

physical_devices = tf.config.list_physical_devices('GPU') 
tf.config.experimental.set_memory_growth(physical_devices[0], True)

batch_size = 16
num_classes = 20
input_shape = (128, 128, 3)

loss_names = ['BCE', 'CCE']
activation={'BCE': tf.keras.activations.sigmoid, 'CCE': None}
from_logits=dict((loss, activation[loss] is None) for loss in loss_names)
bce_logits = from_logits['BCE']
cce_logits = from_logits['CCE']
used_losses = {'BCE': bce(from_logits=cce_logits), 'CCE': cce(from_logits=bce_logits)}

class_weights = [None, 'balanced', 'batch_balanced']
lrs = [1e-3, 5e-4, 1e-4, 5e-5]
layer_oss = ['8-4', '16-8', '16-4']
layer_index = 2

used_metrics = {
    'CCE': ['accuracy', ArgmaxMeanIoU(num_classes), mean_iou(cce_logits), f1_score(cce_logits)], 
    'BCE': ['accuracy', BinaryMeanIoU(from_logits=bce_logits), 
            mean_iou(bce_logits, True), f1_score(bce_logits, True)]
}

def train_and_save_models():
    for loss_name in loss_names:
        for structure in ['decoder', 'enc-dec']:
            for layer_index in [1,2]:
                for cw in ['global_balanced', 'balanced', None]:
                    for lr in [5e-5, 5e-4]:
                        if structure == 'enc-dec' and  cw != 'global_balanced':
                            continue
                        
                        wrapper = DeepLabV3Wrapper(input_shape, used_losses[loss_name], used_metrics[loss_name], 
                                                   batch_size, 2 if loss_name == 'BCE' else num_classes,
                                                   KAMN2_MODEL_IDS[layer_index], activation[loss_name], 
                                                   128, freeze_encoder=(structure == 'decoder'), class_weights=cw)
                        
                        model_path = f'hp/KAMN2/{structure}/{layer_oss[layer_index]}/128/tr/{loss_name}/{cw}/{lr}'
                        
                        print(model_path)
                        wrapper.train(model_path, lr, 20 if loss_name == 'CCE' else 10)
	
def load_keras_model(path):
    return tf.keras.models.load_model(path, custom_objects={
        'ArgmaxMeanIoU': ArgmaxMeanIoU,
        'BinaryMeanIoU': BinaryMeanIoU,
        'mean_iou': mean_iou,
        'f1_score': f1_score,
        'F': f1_score(from_logits, True),
        'J': mean_iou(from_logits, True),
        'dice_loss_f': dice_loss,
        'jaccard_loss_f': jaccard_loss,
        'combined_loss_f': combined_loss
    })

 
def model_is_binary(model):
    backslash_split = model.split('\\')
    forwardslash_split = model.split('/')
    return (len(backslash_split) > 7 and backslash_split[7] == 'BCE') or \
       (len(forwardslash_split) > 7 and forwardslash_split[7] == 'BCE')
 			
def retrain_models(models, dataset='feet', method='fine_tune', lr=5e-8):
    wrapper = DeepLabV3Wrapper(input_shape, None, [], batch_size, build_model=False)
    for model in models:
        wrapper.model = tf.saved_model.load()
        
        loss_name = 'BCE' if model_is_binary(model) else 'CCE'
        wrapper.used_loss = used_losses[loss_name] 
        wrapper.used_metrics = used_metrics[loss_name]

        wrapper.generate_generators(dataset)
        
        print(f'{model} : {method}')
        if method == 'fine_tune':
            wrapper.train(f'{model}_fine_tune', lr, 20)
        elif wrapper.freeze_backbone(method) is not None:
            wrapper.train(f'{model}_{method}', lr, 20)
            wrapper.model.trainable = True
            

    # model_path = f'{base}/test/cce'
    # print(f'{model_path}:')
    # wrapper.train(model_path, used_lr, 1)

    # wrapper = DeepLabV3Wrapper(``
    #     input_shape, bce(from_logits=from_logits), used_metrics, batch_size, 
    #     2, model_id=KAMN2_MODEL_IDS[layer_index], class_weights=cw)

    # model_path = f'{base}/test/bce'
    # print(f'{model_path}:')
    # wrapper.train(model_path, used_lr, 1)       

def show_model_predictions(models, imgs_to_predict='val', save_type='keras_model'):
    wrapper = DeepLabV3Wrapper(input_shape, None, [], batch_size,
                               num_classes, None, build_model=False)
    
    for model in models:
        if model_is_binary(model):
            wrapper.num_classes = 2
        else:
            wrapper.num_classes = num_classes
        wrapper.generate_generators()
        
        if save_type in ['keras', 'keras_model']:
            wrapper.model = tf.keras.models.load_model(model, custom_objects={
                'ArgmaxMeanIoU': ArgmaxMeanIoU,
                'BinaryMeanIoU': BinaryMeanIoU,
                'mean_iou': mean_iou,
                'f1_score': f1_score,
                'F': f1_score(from_logits, True),
                'J': mean_iou(from_logits, True),
                'dice_loss_f': dice_loss,
                'jaccard_loss_f': jaccard_loss,
                'combined_loss_f': combined_loss
            })
        else:
            wrapper.model = tf.saved_model.load(model)
        
        print(model)
        if imgs_to_predict in ['val', 'trainval']:
            wrapper.predict_masks(save_type=save_type)
        if imgs_to_predict in ['train', 'trainval']:
            wrapper.predict_masks(use_val=False, save_type=save_type)
        
def view_saved_model_history(metrics_to_plot='trainval'):
    paths = glob.glob('models\\KAMN2' + '\\*' * 3 + '\\CCE' + '\\*' * 3 + '\\hist.npy')
    
    # metrics = ['val_loss', 'val_accuracy', 'val_J', 'val_F', 'val_argmax_mean_io_u']
    # best_models = dict([(metric, '') for metric in metrics])
    # best_values = dict([(metric, 0.0) for metric in metrics])
    # best_values['val_loss'] = 10000000.0
    
    for path in paths:
        print(path)
        hist = np.load(path, allow_pickle=True).tolist()
        
        # for metric in metrics:
        #     if metric != 'val_loss':
        #         value = hist.get(metric, [0])[-1]
        #         if value > best_values[metric]:
        #             best_values[metric] = value
        #             best_models[metric] = path
        #     else:
        #         value = hist.get(metric, [10000000.0])[-1]
        #         if value < best_values[metric]:
        #             best_values[metric] = value
        #             best_models[metric] = path
        
        _, ax = plt.subplots()
        for k,v in hist.items():
            is_val = k.startswith('val')
            if metrics_to_plot == 'train' and is_val:
                continue
            if metrics_to_plot == 'val' and not is_val:
                continue
            
            color = (np.random.rand(), np.random.rand(), np.random.rand())
            ax.plot(v, color=color, label=k)
            
        handles, labels = (ax.get_legend_handles_labels())
        ax.legend(handles, labels, bbox_to_anchor=(1.04,1), loc="upper left")
        plt.subplots_adjust(right=0.7)
        
        plt.show()
    # for metric in metrics:
    #     print(f'{metric}\n{best_models[metric]}\n{best_values[metric]}')
    
    
# train_and_save_models()
# predict_with_keras_models(find_models(), 'trainval')
# show_model_predictions(find_models(), save_type='saved_model')
# view_saved_model_history()