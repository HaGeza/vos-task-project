import sys
import cv2
import numpy as np
import tensorflow as tf
from util.paths import find_models

input_size = (128, 128)
output_size = (128, 128)

# Resize image for prediction
def crop_image(image):
    H, W = image.shape[:2]
    W2, H2 = int(W/2), int(H/2)
    if W > H:
        W_start = W2 - H2
        W_end = W_start + H
        return image[:, W_start:W_end]
    if H > W:
        H_start = H - W
        return image[H_start:, :]
    return image

def resize_and_crop(image):
    cropped_image = crop_image(image)
    resized_image = cv2.resize(cropped_image, input_size)
    # image_for_prediction = np.asarray(resized_image).astype(np.float32)
    image_for_prediction = np.expand_dims(resized_image, 0)
    return image_for_prediction

def resize_and_pad(image):
    pass

def resize_keep_ar(image):
    factor = 1/ max(image.shape[:2]) * input_size[0]
    width  = int(image.shape[0] * factor)
    height = int(image.shape[1] * factor)
    image = cv2.resize(image, (height, width))
    return np.expand_dims(image, 0)

paths = find_models('best_models', depth=2)
for i, path in enumerate(paths):
    print(f'{i}: {path}')

prompt_str = f'Choose model by index between 0 and {len(paths)-1}... '
while True:
    try:
        path_index = input(prompt_str)
        path_index = int(path_index)
        assert path_index >= 0 and path_index < len(paths)
        break
    except:
        pass

model_path = paths[path_index]
print(model_path)
model = tf.saved_model.load(model_path)
infer = model.signatures['serving_default']

# TODO: find more elegant solution
is_binary = model_path.find('\\BCE\\') > -1 or model_path.find('/BCE/') > -1

if len(sys.argv) < 2:
    print(f'USE: {sys.argv[0]} <video_file_path>')
    sys.exit(1)

cap = cv2.VideoCapture(sys.argv[1])

while cap.isOpened():
    success, image = cap.read()
    if not success:
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        continue
		# break
	
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # image = resize_and_crop(image)
    image = cv2.resize(image, input_size[:2])
    image = np.expand_dims(image, 0)

    preds = infer(tf.constant(image, dtype=tf.float32))
    key   = list(preds.keys())[0]
    preds = preds[key]
    
    if is_binary:
        preds = np.where(preds > 0.5, 255, 0).astype(np.uint8)
    else:
        preds = np.argmax(preds, axis=-1)
        preds = np.where(preds > 0, 255, 0).astype(np.uint8)
       
    preds = np.expand_dims(preds, axis=-1)
    preds = np.squeeze(preds).astype(np.uint8)
    preds = cv2.resize(preds, output_size, interpolation=cv2.INTER_LINEAR)
    image = np.squeeze(image).astype(np.uint8)
    image = cv2.resize(image, output_size, interpolation=cv2.INTER_LINEAR)

    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    preds = np.moveaxis(np.tile(preds, (3,1,1)), 0, -1) 
    overlapped = cv2.addWeighted(image, 0.5, preds, 0.5, 0.0)

    cv2.imshow('Image', image)
    cv2.imshow('Mask', preds)
    cv2.imshow('Overlapped', overlapped)
    if cv2.waitKey(5) & 0xFF == 27:
        break

cap.release()

# 0   NO
# 1   IDK
# 2   IDK
# 3   NO
# 4   YES - problem with calf raise
# 5   YES - problem with calf raise
# 6   YES - problem with calf raise
# 7
# 8
# 9
# 10
# 11
# 12 
# 13 
# 14
# 15

# 0   EH
# 1   SO-SO
# 2   EH
# 3   Eh-SO
# 4   SO-SO
# 5   Flickers, but not bad
# 6   Some flicker on calf raise
# 7   Bad flicker on top right
# 8   Some flicker on calf raise
# 9   Major flicker on calf raise
# 10  Pretty good (minor flicker of c.r.)
# 11  Bad Flickering
# 12  Some flicker on calf raise
# 13  Random blob, bad
# 14  Major flicker on calf raise
# 15  Major flicker on calf raise
# 16  Some flicker on c.r. and heel stand
# 17  Some flicker on top right
# 18  Somewhat good (minor flicker of c.r.)
# 19  Major flicker on calf raise
# 20  Major flicker on calf raise
# 21  Major flicker on calf raise
# 22  Major flicker on calf raise
# 23  Major flicker on calf raise

