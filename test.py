import tensorflow as tf
from tensorflow.keras.losses import BinaryCrossentropy as bce
from datagenerator import DataGenerator
from wrapper import DeepLabV3Wrapper

wrapper = DeepLabV3Wrapper((128, 128, 3), None, [], 3, dataset='feet', build_model=False)
wrapper.try_data_generator(3)