from enum import Enum

import matplotlib.pyplot as plt
import numpy as np

import tensorflow as tf

from deeplab3 import DeepLabV3KA
from util.losses import *
from util.metrics import *
from util.paths import get_paths
from datagenerator import *


def wrapper_from_file(file):
    custom_objects={
        'ArgmaxMeanIoU': ArgmaxMeanIoU,
        'mean_iou': mean_iou,
        'f1_score': f1_score,
        'dice_loss': dice_loss,
        'jaccard_loss': jaccard_loss
    }

    model = tf.keras.models.load_model(file, custom_objects=custom_objects)
    
    # TODO: get model info for wrapper from loaded model
    wrapper = DeepLabV3Wrapper((128, 128, 3), None, [], 16, 21, build_model=False)
    wrapper.model = model
    return wrapper


KAMN2_MODEL_IDS = [0, 1, 2]
KAMN3S_MODEL_ID = 3

class DeepLabV3Wrapper:
    def __init__(self, input_shape, used_loss, used_metrics, batch_size, num_classes=2, 
                 model_id=KAMN2_MODEL_IDS[0], activation=None, num_filters=256,
                 freeze_encoder=True, fg_bias=1.0, class_weights=None, dataset='hp', 
                 verbose=False, build_model=True):        
        self.input_shape = input_shape
        self.used_loss = used_loss
        self.used_metrics = used_metrics
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.class_weights = class_weights
        self.fg_bias = fg_bias
        
        self.generate_generators(dataset)

        if build_model:
            if model_id in KAMN2_MODEL_IDS:
                self.model = DeepLabV3KA(input_shape, num_classes, num_filters, 
                                         model_id, freeze_encoder, activation)
                
            if verbose:
                self.model.summary()
            

    def generate_generators(self, dataset='hp'):
        if self.class_weights not in ACCEPTED_CLASS_WEIGHTS:
            raise ClassWeightNotSupported(self.class_weights)
        if self.class_weights is None and self.fg_bias != 1.0:
            self.class_weights = get_class_weight(self.num_classes, self.fg_bias)
        elif self.class_weights == 'global_balanced':
            self.class_weights = np.array(get_global_sws(dataset))
        paths = get_paths(dataset)
        
        self.train_gen = DataGenerator(
			paths['train_ids'], paths['train_imgs'], paths['train_masks'], self.batch_size,
            self.input_shape, self.num_classes, self.class_weights,
            1.0, True, 0.5, 0.0, 0.5, 0.5, [0.5, 0.2], [0.5, 0.2], 0.5
		)
        self.val_gen = DataGenerator(
			paths['val_ids'], paths['val_imgs'], paths['val_masks'], self.batch_size,
            self.input_shape, self.num_classes, self.class_weights, self.fg_bias,
		)
    

    def freeze_backbone(self, last_layer='expanded_conv_16_project_BN'):
        layer_found = False
        for layer in self.model.layers:
            layer.trainable = False
            if layer.name == last_layer:
                layer_found = True
                break
        
        if not layer_found:
            print(f'Layer {last_layer} not found')
            self.model.trainable = True


    def train(self, model_name, lr, epochs, plot=False):
        models_path = 'models'
        history_path = 'history'
        
        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=lr),
            loss=self.used_loss, metrics=self.used_metrics,
            sample_weight_mode='temporal' if self.class_weights is not None else None
        )

        callbacks = [
            tf.keras.callbacks.TensorBoard(
                log_dir=history_path, histogram_freq=5),
            tf.keras.callbacks.ModelCheckpoint(
                filepath=f'{models_path}/{model_name}_checkpoint',
                save_best_model_only=True)
        ]

        history = self.model.fit(self.train_gen, epochs=epochs,
                                 validation_data=self.val_gen,
                                 callbacks=callbacks)

        vals = list(filter(lambda key: key.startswith('val'), list(history.history.keys())))
        model_name = f'{model_name}/' + '_'.join(['{:.3f}'] * len(vals))
        model_name = model_name.format(*[history.history[val][-1] for val in vals])
        
        self.model.save(f'{models_path}/{model_name}')
        np.save(f'{models_path}/{model_name}/hist.npy', history.history)

        if plot:
            for metric in self.used_metrics:
                plt.plot(history.history[metric], label=metric)
                plt.title(f'{model_name}_train')
                plt.plot(
					history.history[f'val_{metric}'], label=f'val_{metric}')
                plt.title(f'{model_name}_val')
                plt.xlabel('Epoch')
                plt.ylabel(metric)
                plt.ylim([0, 1])
                plt.legend(loc='lower right')
            plt.show()
            

    def evaluate(self):
        self.model.compile(
			optimizer=tf.keras.optimizers.Adam(),
			loss=self.used_loss, metrics=self.used_metrics,
			sample_weight_mode='temporal' if self.class_weights is not None else None
		)
        self.model.evaluate(self.val_gen)
       

    def try_data_generator(self, iter_count, show_count=3):
        for i in range(iter_count):
            imgs, masks, sws = self.train_gen[i]
            
            print(imgs.shape, masks.shape, sws.shape)
            if len(masks.shape) > 3 and masks.shape[-1] > 1:
                masks = np.argmax(masks, axis=-1)
            
            shown = 0
            while shown < imgs.shape[0]:
                show_lim = min(imgs.shape[0], shown + show_count)
                _, ax = plt.subplots(show_count, 2)
                
                for i in range(shown, show_lim):
                    ax[i - shown, 0].imshow(imgs[i])
                    ax[i - shown, 1].imshow(masks[i])
                shown = show_lim
                plt.show()
       

    def predict_masks(self, max_show_count=3, use_val=True, save_type='keras_model'):
        if use_val:
            imgs, masks, _ = self.val_gen[0]
        else:
            imgs, masks, _ = self.train_gen[0]

        if save_type in ['keras', 'keras_model']:
            preds = self.model.predict(imgs)
        else:
            infer = self.model.signatures['serving_default']
            preds = infer(tf.constant(imgs, dtype=tf.float32))
            key   = list(preds.keys())[0]
            preds = preds[key]
        
        if len(masks.shape) > 3 and masks.shape[-1] > 1:
            masks = np.argmax(masks, axis=-1)
        if len(preds.shape) > 3:
            if preds.shape[-1] > 1:
                preds = np.argmax(preds, axis=-1)
            else:
                preds = np.where(preds > 0.5, 1, 0)
            
        show_count = min(max_show_count, len(imgs))
        _, ax = plt.subplots(show_count, 3)
        for i in range(show_count):
            ax[i, 0].imshow(imgs[i])
            ax[i, 1].imshow(masks[i])
            ax[i, 2].imshow(preds[i])
        plt.show()
        

    def calc_loss(self, i):
        x, y_true, w = self.train_gen[i]
        normalized_x = (x / 127.5) - 1
        y_pred = self.model.predict(normalized_x)
        
        loss = self.used_loss(y_true, y_pred, sample_weight=w)
        
        print('Number of NaNs in y_true: ', np.sum(np.isnan(y_true)))
        print('Number of NaNs in y_pred: ', np.sum(np.isnan(y_pred)))
        print('Number of NaNs in sample_w: ', np.sum(np.isnan(w)))
        print(loss)
