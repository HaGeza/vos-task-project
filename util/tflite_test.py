import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from datagenerator import DataGenerator

generator = DataGenerator()

def predict_masks_tflite(interpreter, max_show_count=1):
    imgs, masks, _ = generator[0]
    if len(masks.shape) > 3:
        masks = np.argmax(masks, axis=-1)
    normalized_imgs = ((imgs / 127.5) - 1).astype(np.float32)
    # preds = model.predict(normalized_imgs)

    interpreter.set_tensor(input_details[0]['index'], normalized_imgs)
    interpreter.invoke()

    # Retrieve the raw output map.
    raw_prediction = interpreter.tensor(
        interpreter.get_output_details()[0]['index'])()
    preds = np.argmax(raw_prediction, axis=-1)
    
    show_count = min(max_show_count, len(imgs))
    _, ax = plt.subplots(show_count, 3)
    ax[0].imshow(imgs[0])
    ax[1].imshow(masks[0])
    ax[2].imshow(preds[0])
    plt.show()

# Saved with:
# with open(tflite_path, 'wb') as f:
#    f.write(tflite_model)
tflite_path = 'models/deeplabv3plus.tflite'

interpreter = tf.lite.Interpreter(model_path=tflite_path)
interpreter.allocate_tensors()

# Set model input.
input_details = interpreter.get_input_details()

# Get image size - converting from BHWC to WH
input_size = input_details[0]['shape'][2], input_details[0]['shape'][1]

predict_masks_tflite(interpreter)