import numpy as np
import glob
import os

datasets_path = "E:/Egyetem/Thesis/Project/datasets"

voc_path = f'{datasets_path}/VOCdevkit/VOC2012'
voc = {
    'train_ids'   : f'{voc_path}/ImageSets/Segmentation/train.txt',
    'val_ids'     : f'{voc_path}/ImageSets/Segmentation/val.txt',
    'train_imgs'  : f'{voc_path}/JPEGImages',
    'val_imgs'    : f'{voc_path}/JPEGImages',
    'train_masks' : f'{voc_path}/SegmentationClass',
    'val_masks'   : f'{voc_path}/SegmentationClass'
}
hp_path = f'{datasets_path}/HumanParsing/instance-level_human_parsing'
hp = {
    'train_ids'  : f'{hp_path}/Training/train_id.txt',
    'val_ids'    : f'{hp_path}/Validation/val_id.txt',
    'train_imgs' : f'{hp_path}/Training/Images',
    'val_imgs'   : f'{hp_path}/Validation/Images',
    'train_masks': f'{hp_path}/Training/Category_ids',
    'val_masks'  : f'{hp_path}/Validation/Category_ids'
}
feet_path = f'{datasets_path}/generatedFeet'
feet = {
    'train_ids'  : f'{feet_path}/train_ids.txt',
    'val_ids'    : f'{feet_path}/val_ids.txt',
    'train_imgs' : f'{feet_path}/images',
    'val_imgs'   : f'{feet_path}/images',
    'train_masks': f'{feet_path}/masks',
    'val_masks'  : f'{feet_path}/masks'
}


def get_paths(dataset='human-parsing'):    
    if dataset in ['voc', 'VOC', 'voc2012', 'VOC2012', 'pascalVOC']:
        return voc
    elif dataset in ['hp', 'human-parsing', 'HumanParsing', 'HP']:
        return hp
    elif dataset in ['feet', 'gen-feet', 'genFeet', 'generated-feet', 'generatedFeet']:
        return feet
    else:
        #TODO: trhow excpetion
        return None

def find_models(base='models/hp/KAMN2', depth=7):
    paths = glob.glob(base + '/*' * depth)
    paths = list(filter(lambda p: not p.endswith('_checkpoint'), paths))
    paths = np.concatenate([glob.glob(path + '/*') for path in paths])
    
    return list(filter(lambda f: os.path.isdir(f), paths)) 