import numpy as np
from numpy.core.numeric import indices
import tensorflow as tf
from tensorflow.keras.metrics import MeanIoU, Precision, Recall
from sklearn.metrics import confusion_matrix

class ArgmaxMeanIoU(MeanIoU):
    def __init__(self,
				num_classes=None,
				name=None,
				dtype=None):
        super(ArgmaxMeanIoU, self).__init__(num_classes=num_classes, name=name, dtype=dtype)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.math.argmax(y_true, axis=-1)
        y_pred = tf.math.argmax(y_pred, axis=-1)
        return super().update_state(y_true, y_pred, sample_weight)
    
    def get_config(self):
        return super(ArgmaxMeanIoU, self).get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)
    
class BinaryMeanIoU(MeanIoU):
    def __init__(self,
                 num_classes=2,
                 from_logits=False,
                 threshold=0.5,
                 name=None,
                 dtype=None):
        self.from_logits = from_logits
        self.threshold = threshold
        super(BinaryMeanIoU, self).__init__(2, name=name, dtype=dtype)

    def update_state(self, y_true, y_pred, sample_weight=None):
        if self.from_logits:
            y_pred = tf.nn.softmax(y_pred)
        y_pred = tf.where(y_pred > self.threshold, 1, 0)
        return super().update_state(y_true, y_pred, sample_weight)
    
    def get_config(self):
        return super(BinaryMeanIoU, self).get_config()

    @classmethod
    def from_config(cls, config):
        return cls(**config)


def mean_iou(from_logits=False, binary=False):
    def J(y_true, y_pred, sample_weight=1.0):
        y_true = tf.cast(y_true, tf.float32)
        if from_logits:
            y_pred = tf.nn.softmax(y_pred)

        intersection = y_true * y_pred
        union = y_true + y_pred - intersection
        if not binary:
            intersection = tf.reduce_sum(intersection, axis=-1)
            union = tf.reduce_sum(union, axis=-1)
        return tf.reduce_mean(intersection / union * sample_weight)
    
    return J

def f1_score(from_logits=False, binary=False):
    def F(y_true, y_pred, sample_weight=1.0):
        y_true = tf.cast(y_true, tf.float32)
        if from_logits:
            y_pred = tf.nn.softmax(y_pred)

        numerator = y_true * y_pred
        denominator = y_true + y_pred
        if not binary:
            numerator = tf.reduce_sum(numerator, axis=-1)
            denominator = tf.reduce_sum(denominator, axis=-1)
        return tf.reduce_mean(numerator / denominator * sample_weight)
        
    return F

def twersky_dist(beta, from_logits=False, binary=False):
    def T(y_true, y_pred, sample_weight=1.0):
        y_true = tf.cast(y_true, tf.float32)
        if from_logits:
            y_pred = tf.nn.softmax(y_pred)
            
        numerator = y_true * y_pred
        denominator = y_true * y_pred + beta * (1 - y_true) * y_pred + (1 - beta) * y_true * (1 - y_pred)
        if not binary:
            numerator = tf.reduce_sum(numerator, axis=-1)
            denominator = tf.reduce_sum(denominator, axis=-1)

        return tf.reduce_mean(
            tf.reduce_sum(numerator, axis=-1) / tf.reduce_sum(denominator, axis=-1) * sample_weight)
    
    return T