import numpy as np
from util.metrics import mean_iou, f1_score, twersky_dist

def get_class_weight(num_classes, 
                     fg_bias=None,
                     base_val=1.0):
    bg = base_val
    fg_bias = fg_bias if fg_bias != None else num_classes
    fg = base_val * fg_bias
    
    class_weight = np.array([bg] + [fg] * (num_classes - 1))
    return class_weight

def jaccard_loss(from_logits=False):
    def jaccard_loss_f(y_true, y_pred, sample_weights=1.0):
        return 1 - mean_iou(from_logits)(y_true, y_pred, sample_weights)
    return jaccard_loss_f

def dice_loss(from_logits=False):
    def dice_loss_f(y_true, y_pred, sample_weights=1.0):
        return 1 - f1_score(from_logits)(y_true, y_pred, sample_weights)
    return dice_loss_f

def twersky_loss(beta, from_logits=False):
    def twersky_loss_f(y_true, y_pred, sample_weights=1.0):
        return 1 - twersky_dist(beta, from_logits)(y_true, y_pred, sample_weights)
    return twersky_loss_f

def combined_loss(losses, weights, from_logits=False):
    def combined_loss_f(y_true, y_pred):
        return sum([loss(from_logits=from_logits)(y_true, y_pred) * weight 
                    for loss, weight in zip(losses, weights)])
    return combined_loss_f