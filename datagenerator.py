import numpy as np
import tensorflow as tf
from tensorflow import image as tf_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.utils import to_categorical, Sequence
from util.image_loader import load_img

import matplotlib.pyplot as plt
from PIL import Image

ACCEPTED_CLASS_WEIGHTS = [None, 'balanced', 'batch_balanced', 'global_balanced']
def get_global_sws(dataset='hp'):
    if dataset in ['hp', 'human-parsing', 'HumanParsing', 'HP']:
        return [0.01072426, 0.35750141, 0.13273228, 0.82519672, 0.85058410, 0.06601471,
                0.24728489, 0.08194197, 0.91246154, 0.16238935, 0.41532836, 1.11647664,
                0.83525861, 0.14690248, 0.44698662, 0.45412335, 1.93098487, 2.02760770,
                4.11979587, 4.85970427]
    else:
        return None

class ClassWeightNotSupported(Exception):
    def __init__(self, message='Class weight not supported!', weight=None):
        if weight is None:
            self.message = message
        else:
            self.message = f'Class weight <{weight}> not supported!'
        super().__init__(self.message)

class DataGenerator(Sequence):
    def __init__(self, ids_file, in_dir, gt_dir, batch_size, input_shape,
                 num_classes=None, class_weights=None, fg_bias=1.0, shuffle=False, 
                 h_flip=0.0, v_flip=0.0, zoom=0.0, light=0.0, hue=0.0, saturation=1.0, blur=1.0): 
        # you might want to store the parameters into class variables
        self.input_shape = input_shape
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.shuffle = shuffle
        #TODO: throw exception if not 'multi-class' or 'binary'
        if num_classes is not None and num_classes > 2:
            self.num_classes = num_classes
            self.mode = 'multi-class'
        else:
            self.num_classes = 2
            self.mode = 'binary'
        # load the data from the root directory
        self.data, self.labels = self.get_data(ids_file, in_dir, gt_dir)
        self.indices = np.arange(len(self.data))
        self.on_epoch_end()
        # set data augmentation
        self.__set_augmentation(h_flip, v_flip, zoom, light, hue, saturation, blur)
        # set the class weights
        if not isinstance(class_weights, np.ndarray) and class_weights not in ACCEPTED_CLASS_WEIGHTS:
            print('class_weights must be a member of ' +
                  ACCEPTED_CLASS_WEIGHTS +
                  ' or a numpy.ndarray of class weights.')
            raise ClassWeightNotSupported(class_weights)
        self.fg_bias = fg_bias
        self.class_weights = class_weights


    def get_data(self, ids_file, in_dir, gt_dir):
        ids = np.char.array(open(ids_file, 'r').read().splitlines())
        self.data = in_dir + '/' +  ids + '.jpg'
        self.labels = gt_dir + '/' + ids + '.png'
        return self.data, self.labels


    def __len__(self):
        return int(np.floor(len(self.data) / self.batch_size))


    def __set_augmentation(self, h_flip, v_flip, zoom, light, hue, saturation, blur):
        augs = [zoom, light, hue, saturation, blur]
        for i in range(len(augs)):
            try:
                iter(augs[i])
            except TypeError:
                augs[i] = [augs[i], 0.5]
        
        zoom, light, hue, saturation, blur = augs
        
        
        epsilon = 1e-5
        self.horizontal_flip = min(1.0, max(0.0, h_flip))
        self.vertical_flip = min(1.0, max(0.0, v_flip))

        self.zoom = [zoom[0] if zoom[0] < 1.0 else 1 / zoom[0], zoom[1]]
        self.light = [min(1.0, max(0.0, light[0])), light[1]]
        self.hue = [min(1.0, max(0.0, hue[0])), hue[1]]

        self.saturation = [0, 0, saturation[1]]
        self.saturation[0] = max(0.0, saturation[0]) if saturation[0] < 1.0 else 1./saturation[0]
        self.saturation[1] = 1./max(epsilon, saturation[0]) if saturation[0] < 1.0 else saturation[0]

        self.blur = [0, 0, blur[1]]
        self.blur[0] = max(0.0, blur[0]) if blur[0] < 1.0 else 1./blur[0]
        self.blur[1] = 1./max(epsilon, blur[0]) if blur[0] < 1.0 else blur[0]


    def __apply_augmentation(self, x, y):
        x = tf.cast(x, tf.float32)
        y = tf.cast(y, tf.float32)
        
        if self.light[0] and np.random.rand() < self.light[1]:
            factor = np.random.uniform(-self.light[0], self.light[0])
            if factor <= 0.0:
                x = x + x * factor
            else:
                x = x + (255.0 - x) * factor
        
        if self.zoom[0] and np.random.rand() < self.zoom[1]:
            factor = np.random.uniform(self.zoom[0], 1.0)
            x = tf_img.central_crop(x, factor)
            y = tf_img.central_crop(y, factor)
            x = tf_img.resize(x, self.input_shape[:2])
            y = tf_img.resize(y, self.input_shape[:2], method='nearest')
        
        if self.hue[0] and np.random.rand() < self.hue[1]:
            x = tf.clip_by_value(tf_img.random_hue(x, self.hue[0]), 0.0, 255.0)
        
        if self.saturation[0] != 1.0 and np.random.rand() < self.saturation[2]:
            x = tf.clip_by_value(tf_img.random_saturation(x, *self.saturation[:2]), 0.0, 255.0)
        
        if self.blur[0] != 1.0 and np.random.rand() < self.blur[2]:
            x = tf.clip_by_value(tf_img.random_contrast(x, *self.blur[:2]), 0.0, 255.0)
        
        if self.vertical_flip and np.random.rand() < self.vertical_flip:
            x = tf_img.flip_up_down(x)
            y = tf_img.flip_up_down(y)
        
        if self.horizontal_flip and np.random.rand() < self.horizontal_flip:
            x = tf_img.flip_left_right(x)
            y = tf_img.flip_left_right(y)
        
        return tf.cast(x, tf.uint8), tf.cast(y, tf.uint8)
        

    def __get_from_file(self, x_file, y_file):
        x_size = self.input_shape
        y_size = (*self.input_shape[:2], 1)
        
        # Edit tensorflow implementation if needed, to accept color_mode='label'
        # (Nothing has to be done with the opened PIL image, just don't throw ValueError)
        x = img_to_array(load_img(x_file, target_size=x_size, interpolation='bilinear'))
        y = img_to_array(load_img(y_file, target_size=y_size, interpolation='bilinear', color_mode='label'))
        
        return self.__apply_augmentation(x, y)


    def __get_balanced_class_weights(self, y):
        class_weights = np.zeros((self.num_classes))
        unique, counts = np.unique(y, return_counts=True)
        class_weights[unique] = np.prod(y.shape) / (counts * len(counts))
        class_weights[1:] *= self.fg_bias
        return class_weights


    def __get_sample_weights(self, batch_y):
        if isinstance(self.class_weights, np.ndarray):
            return self.class_weights[batch_y]
        
        if self.class_weights is None:
            return np.ones((self.batch_size, *self.input_shape[:-1]))
        
        if self.class_weights == 'balanced':
            sample_weights = np.zeros(batch_y.shape)
            for i, y in enumerate(batch_y):
                sample_weights[i]  = self.__get_balanced_class_weights(y)[y]
            return sample_weights
        
        if self.class_weights == 'batch_balanced':
            return self.__get_balanced_class_weights(batch_y)[batch_y]
        
        return None
         

    def __getitem__(self, index):
        batch_x = np.zeros((self.batch_size, *self.input_shape), dtype=np.uint8)
        batch_y = np.zeros((self.batch_size, *self.input_shape[:2], 1), dtype=np.uint8)
        
        batch_indices = self.indices[index*self.batch_size : (index+1)*self.batch_size]
        for i, files in enumerate(zip(self.data[batch_indices], self.labels[batch_indices])):
            batch_x[i], batch_y[i] = self.__get_from_file(files[0], files[1])
        
        if self.mode == 'multi-class':
            batch_y = np.where(batch_y >= self.num_classes, 0, batch_y)
            sample_weights = self.__get_sample_weights(batch_y)
            batch_y = to_categorical(batch_y, num_classes=self.num_classes)
        elif self.mode == 'binary':
            batch_y = np.where(batch_y > 0, 1, 0)
            sample_weights = self.__get_sample_weights(batch_y)

        return batch_x, batch_y, sample_weights


    def on_epoch_end(self):
        self.indices = np.arange(len(self.data))
        if self.shuffle:
            np.random.shuffle(self.indices)